%global debug_package %{nil}

Name:           authz
Version:        0.1
Release:        28
Summary:        a isula auth plugin for RBAC
License:        Mulan PSL v2
URL:            https://gitee.com/openeuler/authz
Source0:        https://gitee.com/openeuler/authz/repository/archive/v%{version}.tar.gz
BuildRoot:      %{_tmppath}/authz-root

Patch0001:      0001-authz-print-error-instead-of-panic-when-start-server.patch
Patch0002:      0002-fix-safety-build.patch
Patch0003:      0003-enable-external-linkmode-for-cgo-build.patch
Patch0004:      0004-add-compile-option-ftrapv.patch
Patch0005:      authz-sw.patch

#Dependency
BuildRequires: golang >= 1.8
BuildRequires: glibc-static
#Ensure systemd macros
BuildRequires: systemd

%description
Work with isulad daemon that enables TLS. It brings the support of RBAC.

#Build sections
%prep
%setup -n %{name} -q
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%ifarch sw_64
%patch5 -p1
%endif

%build
make
strip bin/authz-broker

%install
mkdir -p $RPM_BUILD_ROOT/usr/lib/isulad/
mkdir -p $RPM_BUILD_ROOT/lib/systemd/system/
mkdir -p $RPM_BUILD_ROOT/var/lib/authz-broker/

cp bin/authz-broker $RPM_BUILD_ROOT/usr/lib/isulad/
cp systemd/authz.service $RPM_BUILD_ROOT/lib/systemd/system/

chmod 0750 $RPM_BUILD_ROOT/usr/lib/isulad/authz-broker

#Install and uninstall scripts
%pre

%preun
%systemd_preun authz

%post
if [ ! -d "/var/lib/authz-broker" ]; then
	mkdir -p /var/lib/authz-broker
fi
chmod 0750 /var/lib/authz-broker
if [ ! -f "/var/lib/authz-broker/policy.json" ]; then
	cat > /var/lib/authz-broker/policy.json << EOF
{"name":"policy_root","users":[""],"actions":[""]}
EOF
fi
chmod 0640 /var/lib/authz-broker/policy.json

%postun

#Files list
%files
%attr(550,root,root) /usr/lib/isulad
%attr(550,root,root) /usr/lib/isulad/authz-broker
%attr(640,root,root) /lib/systemd/system/authz.service

#Clean section
%clean
rm -rfv %{buildroot}

%changelog
* Wed Dec 14 2022 yangjiaqi <yangjiaqi16@huawei.com> - 0.1-28
- add systemd buildRequires

* Tue Oct 18 2022 wuzx<wuzx1226@qq.com> - 0.1-27
- add sw64 patch

* Fri Sep 30 2022 yangjiaqi <yangjiaqi16@huawei.com> - 0.1-26
- Type:bugfix
- CVE:NA
- SUG:restart
- DESC:optimize compile option

* Mon Mar 22 2021 zhangsong234 <zhangsong34@huawei.com> - 0.1-25
- enable external linkmode for cgo build 

* Fri Mar 19 2021 zhangsong234 <zhangsong34@huawei.com> - 0.1-24
- fix safety build 

* Thu Mar 04 2021 zhangsong234 <zhangsong34@huawei.com> - 0.1-23
- bump version to 0.1-23 

* Mon Mar 01 2021 zhangsong234 <zhangsong34@huawei.com> - 0.1-3
- pirnt error instead of panic when start server 

* Mon Sep 07 2020 wangkang101 <873229877@qq.com> - 0.1-2
- modify url of source0

* Fri Jul 03 2020 zhangsong234 <zhangsong34@huawei.com> - 0.1-1
- release version 0.1
